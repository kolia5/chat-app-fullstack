import { IUser } from "./user-type";

export type UsersState = {
    currentUser: IUser | null;
    users: IUser[] | []
}
