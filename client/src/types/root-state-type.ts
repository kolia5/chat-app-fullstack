import { ChatState } from './chat-state-type';
import { UsersState } from './users-state-type';

export type RootState = {
    chat: ChatState;
    users: UsersState;
}