const ActionsType = {
    GET_ALL_USERS: 'get-all-users',
    LOGIN: 'login'
}

export { ActionsType };