import { createAction, createAsyncThunk } from '@reduxjs/toolkit';

import { ActionsType } from './actions-type';
import { ChatState } from 'types/chat-state-type';
import { RootState } from 'types/root-state-type';
import { IMessage } from 'types/message-type';
import { IMessageService } from 'services/message-service';

import MessageService from 'services/message-service';

import { generateOwnMessage } from 'helpers/generate-own-message';

const messageService: IMessageService = new MessageService();

const toggleModal = createAction<boolean>(ActionsType.TOGGLE_MODAL);
const setEdited = createAction<IMessage>(ActionsType.SET_EDITED);
const cancelEdited = createAction<null>(ActionsType.CANCEL_EDITED);

const getAllMessages = createAsyncThunk<IMessage[], string>(ActionsType.GET_ALL_MESSAGE, 
    async(_, thunkAPI ) => {
        const messages = await messageService.getAllMessages();
        return messages
    }
)

const addNewMessage = createAsyncThunk<IMessage, string>(ActionsType.ADD_NEW, 
    async(text) => {
        const userId = sessionStorage.getItem('userId')
        const message = generateOwnMessage(text, userId!);
        const res = await messageService.addNewMessage(message);
        return res;
    }
)

const editMessage = createAsyncThunk<IMessage, IMessage>(ActionsType.EDIT,
    async(data) => {
        const res = await messageService.editMessage(data);
        return res
    }
)
const deleteMessage = createAsyncThunk<IMessage, string>(ActionsType.DELETE,
    async(messageId) => {
        const res = await messageService.deleteMessage(messageId);
        return res
    }
)

export { 
    getAllMessages, 
    addNewMessage,
    editMessage,
    deleteMessage, 
    toggleModal, 
    setEdited, 
    cancelEdited 
}


