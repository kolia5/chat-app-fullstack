import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const useCheckLogin = () => {
    const navigate = useNavigate();
    const userId = sessionStorage.getItem('userId')
    useEffect(() => {
        !userId && navigate('/login');
    }, [userId])
}

export { useCheckLogin };



