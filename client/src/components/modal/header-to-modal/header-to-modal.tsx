import { CloseIcon } from '../../icons';

interface HeaderToModalProps {
    title: string;
    buttonClassName: string;
    onClose: () => void;
}

const HeaderToModal: React.FC<HeaderToModalProps> = ({ title, buttonClassName, onClose }) => {
    return (
        <div className='header-to-modal'>
            <p>
                { title }
            </p>
            <CloseIcon className={buttonClassName} onClick={onClose}/>
        </div>
        
    )
}
export default HeaderToModal;