import { useEffect } from "react";
import { Routes, Route, Link } from "react-router-dom";
import Login from 'components/login/login';
import Chat from 'components/chat/chat';
import UsersList from 'components/users-list/users-list';
import EditUser from 'components/edit-user/edit-user';
import NotFound from 'components/not-found/not-found';

import 'components/app.css';

const App: React.FC = () => (
     <Routes>
          <Route path='/login' element={<Login/>}/>
          <Route path='/' element={<Chat/>}/>
          <Route path='/users' element={<UsersList/>} />
          <Route path='/users/edit/:id' element={<EditUser/>} />
          <Route path='*' element={<NotFound/>} />
     </Routes>
)
     

export default App;