import { useEffect, useMemo, useState } from 'react';
import { useAppSelector, useAppDispatch } from 'hooks/store-hooks';
import { useCheckLogin } from 'hooks/use-check-login';
import { users as  UserActions} from 'store/actions';

import UserItem from './user-item/user-item';

import './users-list.scss';

const UsersList: React.FC = () => {
    const [ isModal, setIsModal ] = useState<boolean>(false);
    const users = useAppSelector(state => state.users.users);
    const dispatch = useAppDispatch();

    const handleClick = () => {
        setIsModal(s => !s)
    }

    useCheckLogin();

    useEffect(() => {
        dispatch(UserActions.getAllUsers());
    }, [dispatch]);

    const filteredUsers = users.filter(item => item.role === 'user')
    const usersList: JSX.Element[] = filteredUsers.map(user => {
        return (
            <li key={user.id} className='item'>
                <UserItem user={user}/>
            </li>
        )
    }) 
    
    return(
        <div className='users-list'>
            <button className='add-user' onClick={handleClick}>Add New User</button>
            <ul className='users'>
                {usersList}
            </ul>
        </div>
    )
}
export default UsersList