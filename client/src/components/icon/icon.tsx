import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconName } from '@fortawesome/fontawesome-common-types';

interface IconProps {
    name: IconName;
    className?: string;
}

const Icon: React.FC<IconProps> = ({ name, className }) => {
    
    return <FontAwesomeIcon
                icon={name}
                className={className}
            />
}
export default Icon;