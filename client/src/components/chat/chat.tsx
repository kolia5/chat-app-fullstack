import { KeyboardEvent, KeyboardEventHandler, useCallback, useEffect } from 'react';
import { useAppSelector, useAppDispatch } from 'hooks/store-hooks';
import { chat as chatActions } from 'store/actions';

import { useCheckLogin } from 'hooks/use-check-login';

import Modal from 'components/modal/modal';
import EditMessage from 'components/edit-message/edit-message';
import Header from 'components/header/header';
import Preloader from 'components/preloader/preloader';
import ErrorMessage from 'components/error-message/error-message';
import MessageList from 'components/message-list/message-list';
import MessageInput from 'components/message-input/message-input';

import './chat.scss';

const Chat: React.FC = () => {

    const dispatch = useAppDispatch();
    const { messages, preloader, error } = useAppSelector(state => state.chat);
    const editModal = useAppSelector(state => state.chat.editModal);

    const handleCloseModal = () => {
        dispatch(chatActions.toggleModal(false));
        dispatch(chatActions.cancelEdited(null));
    }
       
    useCheckLogin();

    useEffect(() => {
        dispatch(chatActions.getAllMessages(''))
    }, [dispatch]);

   
    if(error){
        return <ErrorMessage/>
    }

    return (
        <div className="chat">
            <Modal isOpen={editModal} onClose={handleCloseModal}>
                <EditMessage onClose={handleCloseModal}/>
            </Modal>
            <Header messages={messages}/>
            {preloader && <Preloader/>}
            { messages.length > 0 && <MessageList messages={messages}/> }
            <MessageInput/>
        </div>
    )
}
export default Chat;
