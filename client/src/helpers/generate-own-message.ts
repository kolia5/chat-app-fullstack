import { v4 as uuidv4 } from 'uuid';
import { IMessage } from '../types/message-type';


const generateOwnMessage = (text: string, userId: string): IMessage => {
    return {
        id: uuidv4(),
        userId: userId,
        avatar: '',
        user: 'User',
        text: text,
        createdAt: new Date().toISOString(),
        editedAt: ''
    }
}
export { generateOwnMessage };

    